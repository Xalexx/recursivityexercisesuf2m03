package recursivity

import java.util.Scanner

fun reduceNumber(number: Int) : Int {
    var n = number
    while (n !in 1..9){
        n = sumaDigit(n)
        reduceNumber(n)
    }
    return n
}

fun sumaDigit(n: Int) : Int {
    var number = n
    var sum = 0
    while (number > 0) {
        sum += number%10
        number /= 10
    }
    return sum
}

fun main() {
    val sc = Scanner(System.`in`)
    val number = sc.nextInt()

    println(reduceNumber(number))
}