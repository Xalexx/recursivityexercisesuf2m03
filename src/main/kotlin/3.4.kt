package recursivity

import java.util.Scanner

fun main() {
    val sc = Scanner(System.`in`)

    findElementInArrayRecursive(listOf(1, 2, 3, 4, 5, 6, 7, 8, 9, 10), 1, 0)
}

fun findElementInArrayRecursive(list: List<Int>, element: Int, index: Int) {
    if (index == list.lastIndex && list[list.lastIndex] != element) println(false)
    else if (list[index] == element) println(true)
    else {
        findElementInArrayRecursive(list, element, index+1)
    }
}