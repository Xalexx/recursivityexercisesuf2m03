package recursivity

import java.util.Scanner

fun numberOfDigits(n: Int) : Int {
    var counter = 0
    counter = if (n == 0) 1
    else {
        numberOfDigits(n / 10)
        n%10
    }
    return counter
}

fun main() {
    val sc = Scanner(System.`in`)
    val number = sc.nextInt()

    println("El nombre de digites de $number es igual a: ${numberOfDigits(number)}")
}