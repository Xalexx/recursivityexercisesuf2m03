package recursivity

fun main() {
    recorrerArrayRecursive(listOf(1, 2, 3, 4, 5, 6, 7, 8), 0)
}


fun recorrerArrayRecursive(list: List<Int>, index: Int){
    if (index == list.lastIndex) print(" ${list[index]}")
    else {
        print(" ${list[index]}")
        recorrerArrayRecursive(list, index+1)
    }
}