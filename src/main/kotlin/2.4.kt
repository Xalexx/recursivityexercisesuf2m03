package recursivity

import java.util.Scanner

fun isGrowing(number: Int) : Boolean {
    return if (number/10 > 0) {
        number % 10 > (number / 10) % 10 && isGrowing(number/10)
    }
    else true
}

fun main() {
    val sc = Scanner(System.`in`)
    val number = sc.nextInt()

    print(isGrowing(number))
}