package recursivity

import java.util.Scanner

fun main() {
    val sc = Scanner(System.`in`)

    println(factorialRecursive(sc.nextInt()))
}

fun factorialRecursive(number: Int) : Long {
    var result = 1L
    if (number == 1) return result
    else result = number * factorialRecursive(number-1)
    return result
}