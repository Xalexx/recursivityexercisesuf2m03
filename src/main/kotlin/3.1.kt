package recursivity

import java.util.Scanner

fun main() {
    val sc = Scanner(System.`in`)

    println(sumNumbers(sc.nextInt()))
}

fun sumNumbers(number: Int) : Int {
    val result : Int
    if (number == 1) {
        return 1
    } else result = number + sumNumbers(number-1)
    return result
}