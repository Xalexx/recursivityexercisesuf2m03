package recursivity

import java.util.Scanner

fun factorial(n: Long) : Long {
    return if (n > 1) n * factorial(n-1)
           else 1
}

fun main() {
    val sc = Scanner(System.`in`)
    val number = sc.nextInt()

    println("El factorial d'aquest nombre es: ${factorial(number.toLong())}")
}