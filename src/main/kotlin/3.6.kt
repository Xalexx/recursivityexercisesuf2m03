package recursivity

import java.util.*

fun main() {
    val sc = Scanner(System.`in`)
    val number = sc.nextInt()
    println(numberOfDigitsRecursive(number))
}

fun numberOfDigitsRecursive(number: Int) : Int{
    val result = if (number == 0) 1
    else {
        numberOfDigitsRecursive(number / 10)
        number % 10
        }
    return result
}