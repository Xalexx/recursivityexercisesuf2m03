package recursivity

import java.util.Scanner

fun isPerfectPrime(number: Int) : Boolean {
    var bool = true
    var num = number
    if (num !in 1..9){
        num = sumaDigit(num)
        if (!checkPrime(num)) bool = false
        else isPerfectPrime(num)
    } else bool = true
    return bool
}

fun checkPrime(number: Int): Boolean {
    var i = 2
    while (i < number && number%i != 0) i++
    return i == number
}

fun main() {
    val sc = Scanner(System.`in`)
    val number = sc.nextInt()

    println(isPerfectPrime(number))
}