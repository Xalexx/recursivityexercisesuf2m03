package recursivity

import java.util.Scanner

fun dobleFactorial(n: Long) : Long {
    return if (n > 1) n * dobleFactorial(n-2)
    else return 1
}

fun main() {
    val sc = Scanner(System.`in`)
    val number = sc.nextLong()

    println("El doble factorial d'aquest nombre es: ${dobleFactorial(number)}")
}