package recursivity

fun main() {

    val matrix = listOf(
        listOf(1, 2, 3, 4),
        listOf(5, 6, 7, 8),
        listOf(9, 10, 11, 12),
        listOf(13, 14, 15, 16)
    )
    recorrerMatrizRecursive(matrix, 0, 0)
}

fun recorrerMatrizRecursive(list: List<List<Int>>, firstIndex: Int, secondIndex: Int) {
    var scndIndex = secondIndex
    if (firstIndex == list.size-1 && scndIndex == list.size-1) print(" ${list[firstIndex][scndIndex]}")
    else {
        if (secondIndex == list.size-1) {
            print(" ${list[firstIndex][secondIndex]}")
            scndIndex = 0
            println()
            recorrerMatrizRecursive(list, firstIndex+1, scndIndex)
        } else {
            print(" ${list[firstIndex][scndIndex]}")
            recorrerMatrizRecursive(list, firstIndex, scndIndex+1)
        }
    }
}